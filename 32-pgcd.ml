let rec gcd a b =
  match (a, b) with
  | (a, b) when a < b -> gcd a (b-a) 
  | (a, b) when a > b -> gcd (a-b) b 
  | _ -> a ;;