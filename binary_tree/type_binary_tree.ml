type 'a binary_tree_t =
  | Empty
  | Node of 'a * 'a binary_tree_t * 'a binary_tree_t ;;
