let rec slice list start_index end_index = 
  match list with
  | [] -> []
  | e::tail when start_index < 0 && end_index > 0 -> e::(slice tail (start_index - 1) (end_index - 1))
  | _::tail -> slice tail (start_index - 1) (end_index - 1) 
;;
