
let rec append x l n = match n with
  | 0 -> l
  | a -> append x (x::l) (a-1)
;;

let rec decode l =
  match l with
  | [] -> []
  | (a, b)::r -> (append b [] a)@decode r
 ;;
