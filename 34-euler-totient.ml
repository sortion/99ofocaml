let rec gcd a b =
  match (a, b) with
  | (a, b) when a < b -> gcd a (b-a) 
  | (a, b) when a > b -> gcd (a-b) b 
  | _ -> a ;;

let coprime a b = gcd a b = 1 ;;

let coprime_int a b = if coprime a b then 1 else 0

