let rec dropped_ieth l n i =
  match l with
  | [] -> []
  | _::r when i mod n = 0 -> dropped_ieth r n (i+1)
  | a::r -> a::dropped_ieth r n (i+1) ;;  
let drop l n = dropped_ieth l n 1 ;; 